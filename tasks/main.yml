---
- name: install required packages
  ansible.builtin.apt:
    pkg:
      - dput
      - devscripts
      - gnupg
      - haveged      # To ensure we have entropy
      - libwww-perl  # For HEAD in fetch-source
      - rebuildd
      - sbuild       # For sbuild group

## Setup the directories

- name: create dedicated user
  ansible.builtin.user:
    name: '{{ rebuildd_user }}'
    home: '{{ rebuildd_homedir }}'
    createhome: yes
    groups:
      - sbuild
    generate_ssh_key: yes
    ssh_key_bits: 4096
    ssh_key_type: 'rsa'

- name: create required directories
  ansible.builtin.file:
    path: '{{ item }}'
    state: directory
    recurse: yes
    owner: '{{ rebuildd_user }}'
  loop:
    - '{{ rebuildd_homedir }}/bin'
    - '{{ rebuildd_homedir }}/build/logs'
    - '{{ rebuildd_homedir }}/build/work'
    - '{{ rebuildd_homedir }}/db'
    - '{{ rebuildd_homedir }}/morgue'

## Configure rebuildd and its associated scripts

- name: copy buildd helper scripts
  ansible.builtin.template:
    src: '{{ item.src }}'
    dest: '{{ rebuildd_homedir }}/bin/{{ item.name }}'
    mode: 0755
    owner: '{{ rebuildd_user }}'
  loop:
    - name: 'fetch-source'
      src: '{{ rebuildd_fetch_script }}'
    - name: 'build'
      src: '{{ rebuildd_build_script }}'
    - name: 'upload-result'
      src: '{{ rebuildd_upload_script }}'
    - name: 'gen-gpg-key'
      src: 'gen-gpg-key.j2'
    - name: 'clean-morgue'
      src: 'clean-morgue.j2'
    - name: 'export-data'
      src: 'export-data.j2'

- name: setup cron entry for clean-morgue
  ansible.builtin.cron:
    name: rebuildd-daily-cleanup
    minute: "42"
    hour: "3"
    user: "{{ rebuildd_user }}"
    job: "{{ rebuildd_homedir }}/bin/clean-morgue >/dev/null"

- name: install rebuildd configuration
  ansible.builtin.template:
    src: rebuilddrc.j2
    dest: /etc/rebuildd/rebuilddrc
    mode: 0644
  notify:
    - restart rebuildd

- name: init rebuildd db
  ansible.builtin.command:
    cmd: rebuildd init
    creates: '{{ rebuildd_homedir }}/db/rebuildd.db'
  become: yes
  become_user: '{{ rebuildd_user }}'

- name: install rebuildd.service
  ansible.builtin.template:
    src: rebuildd.service.j2
    dest: /etc/systemd/system/rebuildd.service
  notify:
    - reload systemd
    - enable rebuildd
    - restart rebuildd

## Setup the GPG key and what's required to upload

- name: generate the gpg key and record it in .devscripts
  ansible.builtin.command:
    cmd: "{{ rebuildd_homedir }}/bin/gen-gpg-key"
    creates: "{{ rebuildd_homedir }}/.devscripts"
  become: yes
  become_user: "{{ rebuildd_user }}"

- name: create .dput.cf
  ansible.builtin.template:
    src: '{{ rebuildd_dput_template }}'
    dest: '{{ rebuildd_homedir }}/.dput.cf'
    mode: 0644
    owner: '{{ rebuildd_user }}'

## Setup ~/.ssh/authorized_keys to allow to remotely enqueue build jobs

- name: configure ssh keys allowed to enqueue build jobs
  ansible.posix.authorized_key:
    user: '{{ rebuildd_user }}'
    comment: '{{ item.comment }}'
    key: '{{ item.ssh_key }}'
    key_options: 'no-pty,no-port-forwarding,no-agent-forwarding,no-X11-forwarding,command="/usr/sbin/rebuildd-job add"'
  loop: '{{ rebuildd_ssh_keys_to_add_jobs }}'
  loop_control:
    label: '{{ item.comment }}'
